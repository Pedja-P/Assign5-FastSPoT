//
//  RZDNetworkActivityIndicatorManager.h
//
//  This class manages Network Activity Indicator keeping track
//  how many times setNetworkActivityIndicatorVisible was called
//
//  Created by Predrag Pavlovic on 8/30/13.
//
//

#import <Foundation/Foundation.h>

@interface RZDNetworkActivityIndicatorManager : NSObject

+ (RZDNetworkActivityIndicatorManager *)sharedNetworkActivityIndicatorManager;
- (void)setNetworkActivityIndicatorVisible:(BOOL)setVisible;

@end
