//
//  RZDNetworkActivityIndicatorManager.m
//
//  This class manages Network Activity Indicator keeping track
//  how many times setNetworkActivityIndicatorVisible was called
//
//  Created by Predrag Pavlovic on 8/30/13.
//
//

#import "RZDNetworkActivityIndicatorManager.h"

@implementation RZDNetworkActivityIndicatorManager

// Class constructor methods should return instancetype to get the type-checking benefit
// (alloc and new already has type-checking)
// Instance methods beginning with init or self compiler automatically promotes, but it is
// still good practice to do it explicitly
// For singletons it is better to use static typing because we usually don't subclass singletons
// -------------------------------------------------------------------------------
//	sharedNetworkActivityIndicatorManager
//  Thread-safe method for creating a singleton
// -------------------------------------------------------------------------------
+ (RZDNetworkActivityIndicatorManager *)sharedNetworkActivityIndicatorManager {
    static dispatch_once_t once;    // A predicate for use with dispatch_once().
                                    // It must be initialized to zero.
                                    // Note: static and global variables default to zero.
    static RZDNetworkActivityIndicatorManager *sharedInstance;
    // executed only once 
    dispatch_once(&once, ^{
        sharedInstance  = [[RZDNetworkActivityIndicatorManager alloc] init];
        // private initialization goes here
    });
    return sharedInstance;
}

// -------------------------------------------------------------------------------
//	setNetworkActivityIndicatorVisible:setVisible
//  Thread-safe method for setting network activity indicator
// -------------------------------------------------------------------------------
- (void)setNetworkActivityIndicatorVisible:(BOOL)setVisible {
    static NSInteger NumberOfCallsToSetVisible;
    @synchronized(self) {   // lock
        if (setVisible) {
            NumberOfCallsToSetVisible++;
        }
        else {
            NumberOfCallsToSetVisible--;
        }
        // The assertion helps to find programmer errors in activity indicator management.
        // Since a negative NumberOfCallsToSetVisible is not a fatal error,
        // it should probably be removed from production code.
        NSAssert(NumberOfCallsToSetVisible >= 0, @"Network Activity Indicator was asked to hide more often than shown");
        
        // Display the indicator as long as our static counter is > 0.
        [UIApplication sharedApplication].networkActivityIndicatorVisible = (NumberOfCallsToSetVisible > 0);
    }
}



// If we don't want to let user creates "normal" instances using init,
// we should override it with assert. In that case we should create
// another designated initializer and call that one in above class method (not init)

@end
