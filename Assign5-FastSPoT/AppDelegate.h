//
//  AppDelegate.h
//  Assign5-FastSPoT
//
//  Created by Predrag Pavlovic on 8/19/13.
//  Copyright (c) 2013 Rezedo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
