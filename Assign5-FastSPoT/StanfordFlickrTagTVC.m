//
//  StanfordFlickrTagTVC.m
//  SPoT
//
//  Created by Predrag Pavlovic on 6/7/13.
//  Copyright (c) 2013 Rezedo. All rights reserved.
//

#import "StanfordFlickrTagTVC.h"
#import "FlickrFetcher.h"

@implementation StanfordFlickrTagTVC

- (NSArray *)ignoredTags
{
    return @[@"cs193pspot", @"portrait", @"landscape"];
}

- (NSArray *)fetchPhotos
{
    return [FlickrFetcher stanfordPhotos];
}

/*
- (void)viewDidLoad
{
    [super viewDidLoad];
	self.photos = [FlickrFetcher stanfordPhotos];
}
 */

@end
