//
//  StanfordFlickrPhotoTVC.m
//  SPoT
//
//  Created by Predrag Pavlovic on 6/16/13.
//  Copyright (c) 2013 Rezedo. All rights reserved.
//

#import "StanfordFlickrPhotoTVC.h"
#import "FlickrFetcher.h"

@implementation StanfordFlickrPhotoTVC

- (NSArray *)sortDescriptors
{
    NSSortDescriptor *sortingByPhotoTitle = [NSSortDescriptor sortDescriptorWithKey:FLICKR_PHOTO_TITLE ascending:YES];
    return @[sortingByPhotoTitle];
}

@end
