//
//  FlickrPhotoTVC.m
//  Shutterbug
//
//  Created by CS193p Instructor.
//  Copyright (c) 2013 Stanford University. All rights reserved.
//

#import "FlickrPhotoTVC.h"
#import "FlickrFetcher.h"
#import "RecentPhotos.h"

@implementation FlickrPhotoTVC

// sets the Model
// reloads the UITableView (since Model is changing)
- (void)setFlickrPhotos:(NSArray *)flickrPhotos
{
    _flickrPhotos = [[flickrPhotos sortedArrayUsingDescriptors:[self sortDescriptors]] copy];
    [self.tableView reloadData];
}

- (NSArray *)sortDescriptors
{
    return nil;
}

- (NSString *)title
{
    if (!super.title) self.title = @"Photos"; // default title (it will be displayed in splitViewBarButtonItem)
    return super.title;
}

- (void)sendDataforIndexPath:(NSIndexPath *)indexPath toViewController:(UIViewController *)vcon
{
    // we don't know the class (we haven't imported it), but we know it should respond to message
    SEL setImageURLSelector = sel_registerName("setImageURL:"); // to suppress the warning "undeclared selector" 
    if ([vcon respondsToSelector:setImageURLSelector]) {
        [RecentPhotos addPhoto:self.flickrPhotos[indexPath.row]];
        NSURL *url = [FlickrFetcher urlForPhoto:self.flickrPhotos[indexPath.row]
                                         format:(self.splitViewController) ? FlickrPhotoFormatOriginal : FlickrPhotoFormatLarge];
        [vcon performSelector:setImageURLSelector withObject:url];
        [vcon setTitle:[self titleForRow:indexPath.row]];
    }
}

#pragma mark - Segue

// prepares for the "Show Image" segue by seeing if the destination view controller of the segue
// understands the method "setImageURL:"
// if it does, it sends setImageURL: to the destination view controller with
// the URL of the photo that was selected in the UITableView as the argument
// also sets the title of the destination view controller to the photo's title.
// Calls transferSplitViewBarButtonItemToViewController:
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender    // only used in iPhone
{
    if ([sender isKindOfClass:[UITableViewCell class]]) {
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        if (indexPath && [segue.identifier isEqualToString:@"Show Image"]) {
            
            // not used, only for iPad, but iPad not using segues here
            [self transferSplitViewBarButtonItemToViewController:segue.destinationViewController];
            
            [self sendDataforIndexPath:indexPath
                      toViewController:segue.destinationViewController];
        }
    }
}

#pragma mark - UITableViewDataSource

// lets the UITableView know how many rows it should display
// in this case, just the count of dictionaries in the Model's array
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.flickrPhotos count];
}

// a helper method that looks in the Model for the photo dictionary at the given row
//  and gets the title out of it
- (NSString *)titleForRow:(NSUInteger)row
{
    return [self.flickrPhotos[row][FLICKR_PHOTO_TITLE] description];
    // description because flickr API
    // could return null (NSNull object) instead of string, description of null is @"[null]"
}

// a helper method that looks in the Model for the photo dictionary at the given row
//  and gets the owner of the photo out of it
- (NSString *)subtitleForRow:(NSUInteger)row
{
    return [[self.flickrPhotos[row] valueForKeyPath:FLICKR_PHOTO_DESCRIPTION] description];
    // description, because could be NSNull
}

// loads up a table view cell with the title and owner of the photo at the given row in the Model
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Flickr Photo";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier
                                                            forIndexPath:indexPath];
    // Configure the cell...
    cell.textLabel.text = [self titleForRow:indexPath.row]; // this would crash if null was sent
    cell.detailTextLabel.text = [self subtitleForRow:indexPath.row]; // this too 
    return cell;
}

#pragma mark - UITableViewDelegate

// instead of segue, sets detail view for iPad (iPhone still using segue not this)
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    id detailVC = [self.splitViewController.viewControllers lastObject];    // only iPad
    if (detailVC) {
        [self sendDataforIndexPath:indexPath toViewController:detailVC];
    }
}

@end
