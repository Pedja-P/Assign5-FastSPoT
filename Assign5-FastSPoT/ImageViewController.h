//
//  ImageViewController.h
//  Shutterbug
//
//  Created by CS193p Instructor.
//  Copyright (c) 2013 Stanford University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SplitViewBarButtonItemPresenter.h"

@interface ImageViewController : UIViewController <SplitViewBarButtonItemPresenter>

// the Model for this VC
// simply the URL of a UIImage-compatible image (jpg, png, etc.)
@property (strong, nonatomic) NSURL *imageURL;

@end
